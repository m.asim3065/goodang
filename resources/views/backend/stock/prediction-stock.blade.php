<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->

<div class="box">
  <div class="box-body">
    <form class="form-inline" method="get">
      <div class="row">
        <div class="col-md-1">
          <span style="font-size: 24px;font-weight: bold">Filter</span>
        </div>
        <div class="col-md-3">
          <select class="form-control" style="width: 100%" name="item">
            <option value="0">- Select Item -</option>
            @foreach($items as $items)
              <option {{(g('item') == $items->id)?'selected':''}} value="{{$items->id}}">{{$items->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-3">
          <select class="form-control" style="width: 100%" name="warehouse">
            <option value="0">- Select Warehouse -</option>
            @foreach($warehouse as $wrs)
              <option {{(g('warehouse') == $wrs->id)?'selected':''}} value="{{$wrs->id}}">{{$wrs->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-3">
          <input type="submit" value="Filter" class="btn btn-success">
        </div>
      </div>
    </form>
  </div>
</div>

@if($id_warehouse)
<div class="box">
  <div class="box-header">
    <p style="font-weight: bold;margin-bottom: 0px">Stock Asset</p>
  </div>
  <div class="box-body">
    <table class="table table-hover table-striped table-bordered">
      <thead>
        <tr>
          <th>Item</th>
          <th>Qty</th>
          <th>Min. Stock</th>
        </tr>
      </thead>
      <tbody>
        @foreach($item as $item)

          <?php 

            if (stockItem($item->id,$id_warehouse) < minimalStock($item->id,$id_warehouse)) {
              $style = 'background-color: #df4a32;color: #fff;';
            }else{
              $style = '';
            }

          ?>

        <tr style="{{$style}}">
          <td>{{$item->name}}</td>
          <td>{{number_format(stockItem($item->id,$id_warehouse),0)}}</td>
          <td>{{number_format(minimalStock($item->id,$id_warehouse),0)}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@else
<div class="box">
  <div class="box-body">
    <center>
      <h3>Please Select Warehouse First</h3>
    </center>
  </div>
</div>
@endif

@endsection()