<!-- First you need to extend the CB layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your custom  HTML goes here -->

<div>
    <p> 
        <a title="Return" href="{{url('admin/barang-masuk-asset')}}">
             <i class="fa fa-chevron-circle-left "></i>
            Back To List Data Barang Masuk
        </a>
    </p>
                    
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong><i class="fa fa-th-list"></i> Add Barang Masuk</strong>
        </div>
        <form id="formAddSave" class="form-horizontal" method="POST" action="{{url('admin/barang-masuk-asset/add-save')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id_item" id="id_item">
            <div class="panel-body" style="padding:20px 0px 0px 0px">       
                <div class="box-body" id="parent-form-area">      
                    <div class="form-group">
                        <label class="control-label col-sm-2">
                            Barcode <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-10">
                            <input type="text" title="Barcode" required class="form-control" name="code" id="code" placeholder="Scan / Type & Enter" value="{{ old('code') }}">
                            <div id="statusBarcode"></div>
                        </div>
                    </div>
                    <div id="wrapDesc">
                        <div class="form-group">
                            <label class="control-label col-sm-2">
                                SKU
                            </label>
                            <div class="col-sm-10">
                                <input type="text" title="SKU" required class="form-control" name="SKU" id="sku" readonly value="{{ old('sku') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">
                                Name
                            </label>
                            <div class="col-sm-10">
                                <input type="text" title="Name" required class="form-control" name="Name" id="name" readonly value="{{ old('name') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">
                                Gudang <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-10">
                                <select class="form-control" name="id_warehouse">
                                        <option>- Pilih Gudang -</option>
                                    @foreach($warehouse as $warehouse)
                                        <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">
                                No PO
                            </label>
                            <div class="col-sm-10">
                                <input type="text" title="No PO" class="form-control" name="no_po" id="no_po" value="{{ old('no_po') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">
                                Description
                            </label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" placeholder="Description" rows="4">{{ old('description') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer" style="background: #F5F5F5">
                    <div class="form-group">
                        <label class="control-label col-sm-2"></label>
                        <div class="col-sm-10">
                            <a href="{{g("return_url")}}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Back</a>

                            <input type="button" name="submitx[]" value="Save &amp; Add More" class="btn btn-success hidden" onclick="submitForm()">
                            <input type="button" name="submitx[]" value="Save" class="btn btn-success" id="submitButton" onclick="submitForm()">
                        </div>
                    </div>
                </div><!-- /.box-footer-->
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    var input = document.getElementById("code");
    input.addEventListener("keyup", function(event) {
      event.preventDefault();
      if (event.keyCode === 13) {
        barcode(input.value);
        return false;
      }
    });
    function barcode(code) {
        var url = '{{url("admin/helpers/asset-detail-from-barcode?barcode=")}}'+code;
        $.get(url,function(response){
           document.getElementById("statusBarcode").innerHTML = response['api_message'];
           if (response['api_status'] == 1) {
            document.getElementById("sku").value     = response['data']['sku'];
            document.getElementById("name").value    = response['data']['name'];
            document.getElementById("id_item").value = response['data']['id_item'];
            $("#submitButton").removeClass('hidden');
           }else{
            document.getElementById("sku").value     = '';
            document.getElementById("name").value    = '';
            document.getElementById("id_item").value = '';
            $("#submitButton").addClass('hidden');
           }
        });
    }

    function submitForm(){
        document.getElementById("formAddSave").submit();
    }
</script>
@endsection()