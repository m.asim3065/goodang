<?php

namespace App\Http\Controllers;

class ApiWarehouseListController extends \crocodicstudio\crudbooster\controllers\ApiController
{
    public function __construct()
    {
        $this->table = 'warehouse_users';
        $this->permalink = 'warehouse_list';
        $this->method_type = 'post';
    }

    public function hook_before(&$postdata)
    {
        //This method will be execute before run the main process
    }

    public function hook_query(&$query)
    {
        //This method is to customize the sql query
    }

    public function hook_after($postdata, &$result)
    {
        //This method will be execute after run the main process

        foreach ($result['data'] as $item) {
            $item->warehouse_name = tv($item->id_warehouse, 'warehouse', 'name');
            $item->warehouse_address = tv($item->id_warehouse, 'warehouse', 'address');
            $item->warehouse_created_at = tv($item->id_warehouse, 'warehouse', 'created_at');
        }
    }
}
