<?php

namespace App\Http\Controllers;

class ApiAssetBarangMasukScanController extends \crocodicstudio\crudbooster\controllers\ApiController
{
    public function __construct()
    {
        $this->table = 'item_asset_code';
        $this->permalink = 'asset_barang_masuk_scan';
        $this->method_type = 'post';
    }

    public function hook_before(&$postdata)
    {
        //This method will be execute before run the main process
        $assetDetailFromBarcode = assetDetailFromBarcode(g('code'), 1);
        // $result['api_status']     = 1;
        // $result['api_message'] = 'Promo Berhasil Di Redeem';
        $result = $assetDetailFromBarcode;
        $res = response()->json($result);
        $res->send();
        exit;
    }

    public function hook_query(&$query)
    {
        //This method is to customize the sql query
    }

    public function hook_after($postdata, &$result)
    {
        //This method will be execute after run the main process
    }
}
